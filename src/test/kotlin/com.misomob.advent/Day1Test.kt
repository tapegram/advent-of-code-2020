package com.misomob.advent

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe


val input = listOf(
    1721,
    979,
    366,
    299,
    675,
    1456,
)

val answer = 514579

class PropertyExample: StringSpec({
    "solution" {
        solution(input) shouldBe  answer
    }
})

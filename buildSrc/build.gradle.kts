repositories {
    jcenter()
}

plugins {
    `kotlin-dsl`
}

dependencies {
    compile(kotlin("stdlib"))
    compile(kotlin("reflect"))
}
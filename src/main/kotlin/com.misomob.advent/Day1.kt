package com.misomob.advent

import arrow.core.*

fun solution(input: List<Int>): Int {
    for (a in input) {
        for (b in input.minusElement(a)) {
            if (2020 == a + b) return a * b
        }
    }

    return 0
}
